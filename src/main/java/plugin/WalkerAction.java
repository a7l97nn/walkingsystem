package plugin;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WalkerAction extends JiraWebActionSupport {
    public WalkerAction() {
    }

    protected String doExecute() {
        return "walkerPage";
    }

    public Map<String,Map<String,String>> bind() throws SearchException {
        List<Issue> issues = new ArrayList<>();
        SearchService searchProvider = ComponentAccessor.getComponentOfType(SearchService.class);
        JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
        builder.where()
                .project("WinterWalk")
                .and()
                .sub()
                .issueType("Request for a walk")
                .endsub()
                .and()
                .assigneeIsCurrentUser()
                .and()
                .status("Binded")
                .endWhere();
        SearchResults results = searchProvider
                .searchOverrideSecurity(JiraManagers.getCurrentUser(), builder.buildQuery(), PagerFilter.getUnlimitedFilter());

        issues = results.getIssues();

        Map<String,Map<String,String>> fieldsMap = new HashMap<>();

        for (Issue issue : issues) {
            issue.getSummary();
            CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
            CustomField customField = customFieldManager.getCustomFieldObject(10206L);
            Map<String,String> map = new HashMap<>();
            map.put(issue.getCustomFieldValue(customField).toString(),issue.getKey());
            fieldsMap.put(issue.getSummary(),map);
        }
        return fieldsMap;
    }
    public Map<String,Map<String,String>> free() throws SearchException {
        List<Issue> issues = new ArrayList<>();
        SearchService searchProvider = ComponentAccessor.getComponentOfType(SearchService.class);
        JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
        builder.where()
                .project("WinterWalk")
                .and()
                .sub()
                .issueType("Request for a walk")
                .endsub()
                .and()
                .assigneeIsEmpty()
                .and()
                .status("Open")
                .endWhere();
        SearchResults results = searchProvider
                .searchOverrideSecurity(JiraManagers.getCurrentUser(), builder.buildQuery(), PagerFilter.getUnlimitedFilter());

        issues = results.getIssues();

        Map<String,Map<String,String>> fieldsMap = new HashMap<>();

        for (Issue issue : issues) {
            issue.getSummary();
            CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
            CustomField customField = customFieldManager.getCustomFieldObject(10206L);
            Map<String,String> map = new HashMap<>();
            map.put(issue.getCustomFieldValue(customField).toString(),issue.getKey());
            fieldsMap.put(issue.getSummary(),map);
        }
        return fieldsMap;

    }
    public Map<String,Map<String,String>> endWalking() throws SearchException {
        List<Issue> issues = new ArrayList<>();
        SearchService searchProvider = ComponentAccessor.getComponentOfType(SearchService.class);
        JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
        builder.where()
                .project("WinterWalk")
                .and()
                .sub()
                .issueType("Request for a walk")
                .endsub()
                .and()
                .assigneeIsCurrentUser()
                .and()
                .status("In progress")
                .endWhere();
        SearchResults results = searchProvider
                .searchOverrideSecurity(JiraManagers.getCurrentUser(), builder.buildQuery(), PagerFilter.getUnlimitedFilter());

        issues = results.getIssues();

        Map<String,Map<String,String>> fieldsMap = new HashMap<>();

        for (Issue issue : issues) {
            issue.getSummary();
            CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
            CustomField customField = customFieldManager.getCustomFieldObject(10206L);
            Map<String,String> map = new HashMap<>();
            map.put(issue.getCustomFieldValue(customField).toString(),issue.getKey());
            fieldsMap.put(issue.getSummary(),map);
        }
        return fieldsMap;
    }
}
