package plugin.api;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.link.DefaultIssueLinkTypeManager;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ImmutableUserIssueRelevance;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowUtil;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import  com.atlassian.jira.ComponentManager;

import javax.ws.rs.Path;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.atlassian.jira.component.ComponentAccessor.getIssueLinkManager;


public class RestController  {
    public RestController()  {
    }

    public String creteIssue(Map<String,String[]> parametrs,String userName) throws NullPointerException {
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(userName);
        IssueService issueService = ComponentAccessor.getIssueService();
        IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
        ProjectManager projectService = ComponentAccessor.getProjectManager();
        Project project = projectService.getProjectByCurrentKey("WIN");

        CustomFieldManager cf = ComponentAccessor.getCustomFieldManager();

        CustomField  gender = cf.getCustomFieldObject(10301L);
        CustomField  color = cf.getCustomFieldObject(10203L);
        CustomField  breed = cf.getCustomFieldObject(10202L);
        CustomField  character = cf.getCustomFieldObject(10204L);

        /*SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        CustomField  date = sdf.format(cf.getCustomFieldObject(10201L));*/
        //CustomField  date = cf.getCustomFieldObject(10201L);


        issueInputParameters.setSummary(parametrs.get("summary")[0])
                .setReporterId(user.getName())
                .setProjectId(project.getId())
                .setIssueTypeId("10202")
                .addCustomFieldValue(gender.getIdAsLong(), parametrs.get("gender")[0])
                .addCustomFieldValue(color.getIdAsLong(), parametrs.get("color")[0])
                .addCustomFieldValue(breed.getIdAsLong(), parametrs.get("breed")[0])
                .addCustomFieldValue(character.getIdAsLong(), parametrs.get("character")[0]);
        IssueService.CreateValidationResult issue;
        issue = issueService.validateCreate(user, issueInputParameters);
        IssueService.IssueResult newIssue;
        try {
             newIssue = issueService.create(user, issue);
        } catch (IllegalStateException e) {
            return "error exception";
        }


        return newIssue.getIssue().getKey()+" Summary:"+newIssue.getIssue().getSummary();
    }

    public String createReq(Map<String,String[]> parametrs,String userName) {
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(userName);
        IssueService issueService = ComponentAccessor.getIssueService();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        Issue issueDog = issueManager.getIssueByCurrentKey(parametrs.get("walkKey")[0]);
        IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
        ProjectManager projectService = ComponentAccessor.getProjectManager();
        Project project = projectService.getProjectByCurrentKey("WIN");

        CustomFieldManager cf = ComponentAccessor.getCustomFieldManager();

        CustomField  startW = cf.getCustomFieldObject(10207L);//start walking
        CustomField  endW = cf.getCustomFieldObject(10208L);//end walking
        CustomField  address = cf.getCustomFieldObject(10206L);//address

        /*SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        CustomField  date = sdf.format(cf.getCustomFieldObject(10201L));*/
        //CustomField  date = cf.getCustomFieldObject(10201L);


        issueInputParameters.setSummary(parametrs.get("summary")[0])
                .setReporterId(user.getName())
                .setProjectId(project.getId())
                .setIssueTypeId("10203")
                .addCustomFieldValue(address.getIdAsLong(), parametrs.get("address")[0]);
                /*.addCustomFieldValue(color.getIdAsLong(), parametrs.get("color")[0])
                .addCustomFieldValue(breed.getIdAsLong(), parametrs.get("breed")[0])
                .addCustomFieldValue(character.getIdAsLong(), parametrs.get("character")[0]);*/
        IssueService.CreateValidationResult issue;
        issue = issueService.validateCreate(user, issueInputParameters);
        IssueService.IssueResult newIssue;
        try {
            newIssue = issueService.create(user, issue);
        } catch (IllegalStateException e) {
            return "error Create";
        }

        IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
        try {
            issueLinkManager.createIssueLink(newIssue.getIssue().getId(), issueDog.getId(), 10300L, null, user);
        } catch (CreateException e) {
            return "errorLink";
        }

        return issueDog.getSummary();
    }

    public String deleteIssue(String key,String userName) {
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(userName);
        IssueService issueService = ComponentAccessor.getIssueService();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        Issue issue = issueManager.getIssueByCurrentKey(key);
        IssueService.DeleteValidationResult deleteValidationResult = issueService.validateDelete(user,issue.getId());
        issueService.delete(user,deleteValidationResult);
        return "Delete";
    }

    public  String editIssue(String key,String userName) {
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(userName);
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        Issue issue = issueManager.getIssueByCurrentKey(key);
        CustomFieldManager cf = ComponentAccessor.getCustomFieldManager();

        CustomField  gender = cf.getCustomFieldObject(10301L);
        CustomField  color = cf.getCustomFieldObject(10203L);
        CustomField  breed = cf.getCustomFieldObject(10202L);
        CustomField  character = cf.getCustomFieldObject(10204L);


        try {
            VelocityEngine velocityEngine = new VelocityEngine();
            VelocityContext velocityContext = new VelocityContext();
            velocityEngine.setProperty("resource.loader", "class");
            velocityEngine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            velocityEngine.init();
            Template template = velocityEngine.getTemplate("/vm/dialogEditPetCard.vm");

            velocityContext.put("issue", issue);
            velocityContext.put("currentUser", user);
            velocityContext.put("cf", cf);
            velocityContext.put("breed", breed);
            velocityContext.put("gender", gender);
            velocityContext.put("color", color);
            velocityContext.put("character", character);
            StringWriter stringWriter = new StringWriter();
            template.merge(velocityContext, stringWriter);


            return stringWriter.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return "err";
        }
    }

    public String nextStatus(String key,String userName){
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(userName);
        IssueService issueService = ComponentAccessor.getIssueService();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        Issue issue = issueManager.getIssueByCurrentKey(key);
        MutableIssue mutableIssue = ComponentAccessor.getIssueManager ().getIssueByCurrentKey(key) ;
        mutableIssue.setAssignee (user) ;
        issueManager.updateIssue (user, mutableIssue, EventDispatchOption.ISSUE_ASSIGNED, true) ;
        boolean result = false;
        IssueService.IssueResult transResult;
        int actionId = 41;

        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();

        IssueService.TransitionValidationResult validationResult = issueService.validateTransition(user, issue.getId(), actionId, issueInputParameters);
        result = validationResult.isValid();
        if (result) {
            transResult = issueService.transition(user, validationResult);
        }
        return "+";
    }

    public String walking(String key,String userName){
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(userName);
        IssueService issueService = ComponentAccessor.getIssueService();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        Issue issue = issueManager.getIssueByCurrentKey(key);
        boolean result = false;
        IssueService.IssueResult transResult;
        int actionId = 51;

        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();

        IssueService.TransitionValidationResult validationResult = issueService.validateTransition(user, issue.getId(), actionId, issueInputParameters);
        result = validationResult.isValid();
        if (result) {
            transResult = issueService.transition(user, validationResult);
        }
        return "+";
    }

    public String endWalking(String key,String userName){
        ApplicationUser user = ComponentAccessor.getUserManager().getUserByKey(userName);
        IssueService issueService = ComponentAccessor.getIssueService();
        IssueManager issueManager = ComponentAccessor.getIssueManager();
        Issue issue = issueManager.getIssueByCurrentKey(key);
        boolean result = false;
        IssueService.IssueResult transResult;
        int actionId = 71;

        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();

        IssueService.TransitionValidationResult validationResult = issueService.validateTransition(user, issue.getId(), actionId, issueInputParameters);
        result = validationResult.isValid();
        if (result) {
            transResult = issueService.transition(user, validationResult);
        }
        return "+";
    }

    public  List<CustomFieldOptions> getOptionsByCuftomfield(String q) throws Exception{
        CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

        Options options = ComponentAccessor.getOptionsManager().getOptions(customFieldManager.getCustomFieldObject(q).getConfigurationSchemes().listIterator().next().getOneAndOnlyConfig());

        List <CustomFieldOptions> cfOptions = new LinkedList<>();
        options.forEach(option -> {
            cfOptions.add(new CustomFieldOptions(option.getValue(), option.getOptionId().toString()));
        });
        return cfOptions;
    }
}
