package plugin.api;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.option.Options;


import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.Arrays;



@Path("/rest")
public class Rest {
    public Rest(){}
    @POST
    //@AnonymousAllowed
    @Path("/createPetCard")
    @Produces({MediaType.APPLICATION_JSON})
    public Response createPetCard(@Context HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        request.getParameterMap().forEach((param, values) -> {
            sb.append(param).append("=").append(Arrays.toString(values));
        });
        RestController restController = new RestController();

        return Response.ok(restController.creteIssue(request.getParameterMap(),request.getRemoteUser())).build();
    }

    @POST
    //@AnonymousAllowed
    @Path("/createReq")
    @Produces({MediaType.APPLICATION_JSON})
    public Response createReq(@Context HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        request.getParameterMap().forEach((param, values) -> {
            sb.append(param).append("=").append(Arrays.toString(values));
        });
        RestController restController = new RestController();

        return Response.ok(restController.createReq(request.getParameterMap(),request.getRemoteUser())).build();
    }

    @GET
    //@AnonymousAllowed
    @Path("/deletePetCard")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deletePetCard(@Context HttpServletRequest request) {
        String requestString = request.getQueryString().split("=")[1];
        RestController restController = new RestController();
        return Response.ok(restController.deleteIssue(requestString,request.getRemoteUser())).build();
    }

    @GET
    //@AnonymousAllowed
    @Path("/statusWalking")
    @Produces({MediaType.APPLICATION_JSON})
    public Response statusWalking(@Context HttpServletRequest request) {
        String requestString = request.getQueryString().split("=")[1];
        RestController restController = new RestController();
        return Response.ok(restController.walking(requestString,request.getRemoteUser())).build();
    }

    @GET
    //@AnonymousAllowed
    @Path("/statusEndWalking")
    @Produces({MediaType.APPLICATION_JSON})
    public Response statusEndWalking(@Context HttpServletRequest request) {
        String requestString = request.getQueryString().split("=")[1];
        RestController restController = new RestController();
        return Response.ok(restController.endWalking(requestString,request.getRemoteUser())).build();
    }

    @GET
    //@AnonymousAllowed
    @Path("/nextStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public Response nextStatus(@Context HttpServletRequest request) {
        String requestString = request.getQueryString().split("=")[1];
        RestController restController = new RestController();
        return Response.ok(restController.nextStatus(requestString,request.getRemoteUser())).build();
    }

    @GET
    //@AnonymousAllowed
    @Path("/editPetCard")
    @Produces({MediaType.APPLICATION_JSON})
    public Response editPetCard(@Context HttpServletRequest request) {
        String requestString = request.getParameter("key");
        RestController restController = new RestController();
        return Response.ok(restController.editIssue(requestString,request.getRemoteUser())).build();
    }


    @POST
   //@AnonymousAllowed
    @Path("/getOptionsByCuftomfieldById")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getOptionsByCuftomfieldById(@QueryParam("q") String q) {
        RestController restController = new RestController();
        try {
            return Response.ok(restController.getOptionsByCuftomfield(q)).build();
        } catch (Exception e) {
            return  Response.noContent().build();
        }
    }
}
