package plugin.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class make a dataset type of filter bean
 * @author <a href="https://erp.netcracker.com/ncobject.jsp?id=9149311758413440476">alla1117</a>
 */
@XmlRootElement(name = "schema")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomFieldOptions {

    /**
     * This field contains a option name
     */
    @XmlElement
    private String name;

    /**
     * This field contains a option id
     */
    @XmlElement
    private String id;

    public CustomFieldOptions(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
